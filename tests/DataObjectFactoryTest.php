<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\DataObject\Test;

use Interactiv4\DataObject\DataObject;
use Interactiv4\DataObject\DataObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class DataObjectFactoryTest.
 *
 * @internal
 */
class DataObjectFactoryTest extends TestCase
{
    /**
     * Test DataObject class exists and is an instance of DataObject.
     */
    public function testInstanceOf(): void
    {
        $dataObjectFactory = new DataObjectFactory();
        $dataObject = $dataObjectFactory->create();
        static::assertInstanceOf(DataObject::class, $dataObject);
    }

    /**
     * Test some basic data manipulation, based on DataObject behaviour.
     */
    public function testDataManipulation(): void
    {
        $testData = ['test' => true];

        // Test empty construct data
        $dataObjectFactory = new DataObjectFactory();
        $dataObject = $dataObjectFactory->create();
        static::assertSame([], $dataObject->getData());

        // Test set array data via setData method
        $dataObject->setData($testData);
        static::assertSame($testData, $dataObject->getData());

        // Test set simple data via setData method
        $dataObject->setData('test', false);
        static::assertFalse($dataObject->getData('test'));
        static::assertFalse($dataObject->getTest());

        // Test insert construct data
        $dataObjectFactory = new DataObjectFactory();
        $dataObject = $dataObjectFactory->create($testData);
        static::assertSame($testData, $dataObject->getData());

        // Test output methods
        static::assertSame($testData, $dataObject->toArray());
        static::assertSame("<item>\n<test><![CDATA[1]]></test>\n</item>\n", $dataObject->toXml());
        static::assertSame('{"test":true}', $dataObject->toJson());
        static::assertSame('test="1"', $dataObject->serialize());
    }
}
