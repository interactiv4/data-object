<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\DataObject\Test\Api;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterfaceFactoryInterface;
use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\DataObject\Api\DataObjectInterfaceFactory;
use Interactiv4\DataObject\DataObjectFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class DataObjectInterfaceFactoryTest.
 *
 * @internal
 */
class DataObjectInterfaceFactoryTest extends TestCase
{
    /**
     * Test DataObjectInterfaceFactory class exists and is an instance of DataObjectInterface.
     */
    public function testInstanceOf(): void
    {
        /** @var ObjectFactoryInterface|MockObject $objectFactory */
        $objectFactory = $this->getMockForAbstractClass(ObjectFactoryInterface::class);
        $dataObjectInterfaceFactory = new DataObjectInterfaceFactory($objectFactory);
        static::assertInstanceOf(DataObjectInterfaceFactoryInterface::class, $dataObjectInterfaceFactory);
    }

    /**
     * Test factory delegates creation.
     */
    public function testFactoryDelegatesCreation(): void
    {
        /** @var ObjectFactoryInterface|MockObject $objectFactory */
        $objectFactory = $this->getMockForAbstractClass(ObjectFactoryInterface::class);
        $dataObjectInterfaceFactory = new DataObjectInterfaceFactory($objectFactory);

        /** @var FactoryInterface|MockObject $factory */
        $factory = $this->getMockForAbstractClass(FactoryInterface::class);

        $factoryArguments = [];
        $objectArguments = ['test' => true];

        $objectFactory->expects(static::once())
            ->method('create')
            ->with(DataObjectFactory::class, $factoryArguments)
            ->willReturn($factory)
        ;

        $factory->expects(static::once())
            ->method('create')
            ->with($objectArguments)
            ->willReturn($this->getMockForAbstractClass(DataObjectInterface::class))
        ;

        $dataObjectInterfaceFactory->create($objectArguments);
    }
}
