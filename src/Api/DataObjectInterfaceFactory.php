<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\DataObject\Api;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterfaceFactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Contracts\Factory\Factory\Proxy;
use Interactiv4\DataObject\DataObjectFactory;

/**
 * Class DataObjectInterfaceFactory.
 *
 * @api
 */
class DataObjectInterfaceFactory extends Proxy implements DataObjectInterfaceFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        ObjectFactoryInterface $objectFactory,
        string $factoryClass = DataObjectFactory::class,
        array $factoryArguments = []
    ) {
        parent::__construct(
            $objectFactory,
            $factoryClass,
            $factoryArguments
        );
    }

    /**
     * Proxy object creation to concrete factory.
     *
     * {@inheritdoc}
     */
    public function create(array $arguments = []): DataObjectInterface
    {
        return parent::create($arguments);
    }
}
