<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\DataObject;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;

/**
 * Universal data container with array access implementation.
 *
 * @api
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class DataObject implements DataObjectInterface
{
    /**
     * Object attributes.
     *
     * @var array
     */
    private $data = [];

    /**
     * Setter/Getter underscore transformation cache.
     *
     * @var array
     */
    private static $underscoreCache = [];

    /**
     * Constructor.
     *
     * By default is looking for first argument as array and assigns it as object attributes
     * This behavior may change in child classes
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function __call(
        string $method,
        array $args
    ) {
        switch (\substr($method, 0, 3)) {
            case 'get':
                $key = $this->underscore(\substr($method, 3));
                $index = $args[0] ?? null;

                return $this->getData($key, $index);
            case 'set':
                $key = $this->underscore(\substr($method, 3));
                $value = $args[0] ?? null;

                return $this->setData($key, $value);
            case 'uns':
                $key = $this->underscore(\substr($method, 3));

                return $this->unsetData($key);
            case 'has':
                $key = $this->underscore(\substr($method, 3));

                return isset($this->data[$key]);
        }
        throw new \RuntimeException(\sprintf('Invalid method %s::%s', \get_class($this), $method));
    }

    /**
     * {@inheritdoc}
     */
    public function addData(array $data): void
    {
        foreach ($data as $index => $value) {
            $this->setData($index, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setData($key, $value = null): void
    {
        switch (\is_array($key)) {
            case true:
                $this->data = $key;
                break;
            case false:
                $this->data[$key] = $value;
                break;
            default:
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function unsetData($key = null): void
    {
        if (null === $key) {
            $this->setData([]);

            return;
        }

        if (\is_string($key)) {
            if (isset($this->data[$key]) || \array_key_exists($key, $this->data)) {
                unset($this->data[$key]);
            }

            return;
        }

        if ($key === (array) $key) {
            foreach ($key as $element) {
                $this->unsetData($element);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getData(string $key = '', $index = null)
    {
        if ('' === $key) {
            return $this->data;
        }

        /* process a/b/c key as ['a']['b']['c'] */
        $data = \strpos($key, '/') ? $this->getDataByPath($key) : $this->getRawData($key);

        if (null !== $index) {
            if ($data instanceof DataObjectInterface) {
                return $data->getData((string) $index);
            }

            if (\is_string($data)) {
                $data = \explode(PHP_EOL, $data);
            }

            if (\is_array($data)) {
                return $data[$index] ?? null;
            }

            return null;
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByPath(string $path)
    {
        $keys = \explode('/', $path);

        $data = $this->data;
        foreach ($keys as $key) {
            if (\is_array($data)) {
                return $data[$key] ?? null;
            }

            if ($data instanceof DataObjectInterface) {
                return $data->getDataByKey($key);
            }

            return null;
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataByKey(string $key)
    {
        return $this->getRawData($key);
    }

    /**
     * {@inheritdoc}
     */
    public function setDataUsingMethod(string $key, $args = []): void
    {
        $method = 'set' . \str_replace(' ', '', \ucwords(\str_replace('_', ' ', $key)));
        $this->{$method}($args);
    }

    /**
     * {@inheritdoc}
     */
    public function getDataUsingMethod($key, $args = null)
    {
        $method = 'get' . \str_replace(' ', '', \ucwords(\str_replace('_', ' ', $key)));

        return $this->{$method}($args);
    }

    /**
     * {@inheritdoc}
     */
    public function hasData($key = ''): bool
    {
        if (empty($key) || !\is_string($key)) {
            return !empty($this->data);
        }

        return \array_key_exists($key, $this->data);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(array $keys = []): array
    {
        if (empty($keys)) {
            return $this->data;
        }

        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->data[$key] ?? null;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToArray(array $keys = []): array
    {
        return $this->toArray($keys);
    }

    /**
     * {@inheritdoc}
     */
    public function toXml(
        array $keys = [],
        string $rootName = 'item',
        bool $addOpenTag = false,
        bool $addCdata = true
    ): string {
        $xml = '';
        $data = $this->toArray($keys);
        foreach ($data as $fieldName => $fieldValue) {
            $fieldValue = (true === $addCdata)
                ? "<![CDATA[{$fieldValue}]]>"
                : \str_replace(
                    ['&', '"', "'", '<', '>'],
                    ['&amp;', '&quot;', '&apos;', '&lt;', '&gt;'],
                    $fieldValue
                );
            $xml .= "<{$fieldName}>{$fieldValue}</{$fieldName}>\n";
        }
        if ($rootName) {
            $xml = "<{$rootName}>\n{$xml}</{$rootName}>\n";
        }
        if ($addOpenTag) {
            $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . $xml;
        }

        return $xml;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToXml(
        array $keys = [],
        string $rootName = 'item',
        bool $addOpenTag = false,
        bool $addCdata = true
    ): string {
        return $this->toXml($keys, $rootName, $addOpenTag, $addCdata);
    }

    /**
     * {@inheritdoc}
     */
    public function toJson(array $keys = [], $options = 0, $depth = 512)
    {
        $data = $this->toArray($keys);

        return \json_encode($data, $options, $depth);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToJson(array $keys = [])
    {
        return $this->toJson($keys);
    }

    /**
     * {@inheritdoc}
     */
    public function toString($format = ''): string
    {
        if (empty($format)) {
            return \implode(', ', $this->getData());
        }
        \preg_match_all('/\{\{([a-z0-9_]+)\}\}/is', $format, $matches);
        foreach ($matches[1] as $var) {
            $format = \str_replace('{{' . $var . '}}', $this->getData($var), $format);
        }

        return $format;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return empty($this->data);
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(
        array $keys = [],
        string $valueSeparator = '=',
        string $fieldSeparator = ' ',
        string $quote = '"'
    ): string {
        $data = [];
        if (empty($keys)) {
            $keys = \array_keys($this->data);
        }

        foreach ($this->data as $key => $value) {
            if (\in_array($key, $keys, true)) {
                $data[] = $key . $valueSeparator . $quote . $value . $quote;
            }
        }

        return \implode($fieldSeparator, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function debug(
        $data = null,
        array &$objects = []
    ): array {
        if (null === $data) {
            $hash = \spl_object_hash($this);
            if (!empty($objects[$hash])) {
                return ['*** RECURSION ***'];
            }
            $objects[$hash] = true;
            $data = $this->getData();
        }
        $debug = [];
        foreach ($data as $key => $value) {
            if (\is_scalar($value)) {
                $debug[$key] = $value;
                continue;
            }

            if (\is_array($value)) {
                $debug[$key] = $this->debug($value, $objects);
                continue;
            }

            if ($value instanceof DataObjectInterface) {
                $debug[$key . ' (' . \get_class($value) . ')'] = $value->debug(null, $objects);
                continue;
            }
        }

        return $debug;
    }

    /**
     * Implementation of \ArrayAccess::offsetSet().
     *
     * @param string $offset
     * @param mixed  $value
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetset.php
     */
    public function offsetSet($offset, $value): void
    {
        $this->data[$offset] = $value;
    }

    /**
     * Implementation of \ArrayAccess::offsetExists().
     *
     * @param string $offset
     *
     * @return bool
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetexists.php
     */
    public function offsetExists($offset): bool
    {
        return isset($this->data[$offset]) || \array_key_exists($offset, $this->data);
    }

    /**
     * Implementation of \ArrayAccess::offsetUnset().
     *
     * @param string $offset
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetunset.php
     */
    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }

    /**
     * Implementation of \ArrayAccess::offsetGet().
     *
     * @param string $offset
     *
     * @return mixed
     *
     * @see http://www.php.net/manual/en/arrayaccess.offsetget.php
     */
    public function offsetGet($offset): mixed
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    /**
     * Get value from $data array without parse key.
     *
     * @param string $key
     *
     * @return mixed
     */
    private function getRawData($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        return null;
    }

    /**
     * Converts field names for setters and getters.
     *
     * $this->setMyField($value) === $this->setData('my_field', $value)
     * Uses cache to eliminate unnecessary preg_replace
     *
     * @param string $name
     *
     * @return string
     */
    private function underscore($name)
    {
        if (isset(self::$underscoreCache[$name])) {
            return self::$underscoreCache[$name];
        }
        $result = \strtolower(\trim(\preg_replace('/([A-Z]|[0-9]+)/', '_$1', $name), '_'));
        self::$underscoreCache[$name] = $result;

        return $result;
    }
}
