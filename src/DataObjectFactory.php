<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\DataObject;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class DataObjectFactory.
 *
 * @api
 */
class DataObjectFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): DataObject
    {
        return new DataObject($data);
    }
}
